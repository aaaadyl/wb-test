import { LoginPage } from "@/src/views/login";

export default function Login() {
  return <LoginPage />;
}

export const metadata = {
  title: "WB Login by Adylbek Ergeshov",
};
