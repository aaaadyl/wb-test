import { MainPage } from "@/src/views/main";

export default function IndexPage() {
  return <MainPage />;
}

export const metadata = {
  title: "WB Test by Adylbek Ergeshov",
};
