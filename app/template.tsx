"use client";

import { appStore, persistedStore } from "@/src/store/appStore";
import { Provider as ReduxProvider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import AuthGuard from "@/src/widgets/auth-guard";

export default function Template({ children }: React.PropsWithChildren) {
  return (
    <ReduxProvider store={appStore}>
      <PersistGate persistor={persistedStore}>
        <AuthGuard>{children}</AuthGuard>
      </PersistGate>
    </ReduxProvider>
  );
}
