/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  images: {
    unoptimized: true,
    domains: ["basket-10.wbbasket.ru"],
  },
};

export default nextConfig;
