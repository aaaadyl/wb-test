import { ROUTES, baseApi } from "@/src/shared/api";
import { type Advert } from "../model/types";
import { AdvertResDto, type AdvertFilter } from "./types";
import { mapAdvert } from "../lib/mapAdvert";
import { mapSubject } from "../lib/mapSubjec";

export const advertApi = baseApi.injectEndpoints({
  endpoints: (build) => ({
    advertisements: build.query<Advert, AdvertFilter>({
      query: (params) => ({
        url: ROUTES.adverts.list,
        params: { ...params, type: 6 },
      }),
      transformResponse: (response: AdvertResDto) => {
        const adverts = response.bets.map(mapAdvert);
        const priorities = response.subject_priorities.map(mapSubject);
        return { adverts, priorities };
      },
      transformErrorResponse: (error) => {
        return error;
      },
    }),
  }),
});

export const { useAdvertisementsQuery } = advertApi;
