export type Subject = {
  id: number;
  name: string;
};

export type AdvertDto = {
  advert_id: string;
  position_on_page: number;
  image_big: string;
  article: number;
  position: number;
  cpm: number;
  subject: Subject;
  delivery_time: number;
  page: number;
};

export type AdvertResDto = {
  bets: AdvertDto[];
  subject_priorities: Subject[];
};

export type AdvertDetailsDto = AdvertDto & {
  detailsImageUrl: string[];
  description: string;
};

export type AdvertFilter = {
  type?: number;
  input?: string;
  region_id?: string;
};

export type AdvertError = {
  input: string;
};

export type AdvertErrorDto = {
  status: number;
  data: AdvertError;
};
