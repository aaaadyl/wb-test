import { AdvertDto } from "../api/types";
import { type AdvertDetails } from "../model/types";

export function mapAdvert(dto: AdvertDto): AdvertDetails {
  return {
    id: dto.advert_id,
    image: dto.image_big,
    place: dto.position,
    article: dto.article,
    position: dto.position_on_page,
    cpm: dto.cpm,
    subject: dto.subject,
    delivery: dto.delivery_time,
    page: dto.page,
  };
}
