import { Subject } from "../api/types";

export function mapSubject(dto: Subject): Subject {
  return {
    id: dto.id,
    name: dto.name,
  };
}
