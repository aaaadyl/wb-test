import { createSlice } from "@reduxjs/toolkit";
import { advertApi } from "../api/advertApi";
import { Advert } from "./types";

type Advertisements =
  | Advert
  | {
      adverts: [];
      priorities: [];
    };

type AdvertisementSlice = Advertisements & {
  loading: boolean;
  error: string | null;
};

const initialState: AdvertisementSlice = {
  adverts: [],
  priorities: [],
  loading: false,
  error: null,
};

export const advertisementsSlice = createSlice({
  name: "advertisements",
  initialState,
  reducers: {
    clearAdvertsData: (state) => {
      state.adverts = [];
      state.priorities = [];
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      advertApi.endpoints.advertisements.matchPending,
      (state: AdvertisementSlice) => {
        state.loading = true;
      }
    );
    builder.addMatcher(
      advertApi.endpoints.advertisements.matchFulfilled,
      (state: AdvertisementSlice, { payload }) => {
        state.adverts = payload.adverts;
        state.priorities = payload.priorities;
        state.loading = false;
        state.error = null;
      }
    );
    builder.addMatcher(
      advertApi.endpoints.advertisements.matchRejected,
      (state: AdvertisementSlice, action) => {
        state.loading = false;
        state.error =
          action?.payload?.status === 400
            ? "WB не вернул данные по рекламе. Проверьте правильность введенных данных и попробуйте позже."
            : null;
      }
    );
  },
});

export const { clearAdvertsData } = advertisementsSlice.actions;
