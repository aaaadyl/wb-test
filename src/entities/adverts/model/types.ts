/**
 * ✅ DX Best Practice
 * Use branded type to entity id to
 * don't to be confused with other identifiers
 */

import { Subject } from "../api/types";

export type Advert = {
  priorities: Subject[];
  adverts: AdvertDetails[];
};

export type AdvertDetails = {
  id: string;
  place: number;
  image: string;
  article: number;
  position: number;
  cpm: number;
  subject: Subject;
  delivery: number;
  page: number;
};
