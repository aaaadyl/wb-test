export {
  setRegionValue,
  setSearchValue,
  clearFiltersData,
  filtersSlice,
} from "./model/slice";
