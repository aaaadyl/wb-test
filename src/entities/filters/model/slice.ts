import { createSlice, type PayloadAction } from "@reduxjs/toolkit";

type filtersState = {
  region: string;
  search: string;
};

const initialState: filtersState = {
  region: "",
  search: "",
};

export const filtersSlice = createSlice({
  name: "filters",
  initialState,
  reducers: {
    clearFiltersData: (state) => {
      state.region = "";
      state.search = "";
    },
    setSearchValue: (state, action: PayloadAction<string>) => {
      state.search = action.payload;
    },
    setRegionValue: (state, action: PayloadAction<string>) => {
      state.region = action.payload;
    },
  },
});

export const { clearFiltersData, setRegionValue, setSearchValue } =
  filtersSlice.actions;
