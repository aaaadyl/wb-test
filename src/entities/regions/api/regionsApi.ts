import { ROUTES, baseApi } from "@/src/shared/api";
import { type Region } from "../model/types";
import { type RegionDto } from "./types";
import { mapAdvert } from "../lib/mapRegions";

export const regionsApi = baseApi.injectEndpoints({
  endpoints: (build) => ({
    regions: build.query<Region[], void>({
      query: () => ({
        url: ROUTES.regions.list,
      }),
      transformResponse: (response: RegionDto[]) => response.map(mapAdvert),
    }),
  }),
});

export const { useRegionsQuery } = regionsApi;
