export type RegionDto = {
  id: string;
  name: string;
};
