import { RegionDto } from "../api/types";
import { type Region } from "../model/types";

export function mapAdvert(dto: RegionDto): Region {
  return {
    id: dto.id,
    name: dto.name,
  };
}
