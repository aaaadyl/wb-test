export type Region = {
  id: string;
  name: string;
};

export type RegionDetails = {
  id: string;
  name: string;
};
