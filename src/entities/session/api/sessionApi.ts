import { SESSION_TAG, baseApi } from "@/src/shared/api";
import { mapSession } from "../lib/mapSession";
import { type Session } from "../model/types";
import { type RequestLoginBody, type SessionDto } from "./types";

export const sessionApi = baseApi.injectEndpoints({
  endpoints: (build) => ({
    login: build.mutation<Session, RequestLoginBody>({
      query: (body) => ({
        url: `/users/login/`,
        method: "POST",
        body,
      }),
      invalidatesTags: [SESSION_TAG],
      transformResponse: (response: SessionDto) => mapSession(response),
    }),
  }),
});

export const { useLoginMutation } = sessionApi;
