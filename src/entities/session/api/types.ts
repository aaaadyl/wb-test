export type SessionDto = {
  access: string;
  refresh: string;
  is_email_confirmed: boolean;
};

export type RequestLoginBody = {
  email: string;
  password: string;
};
