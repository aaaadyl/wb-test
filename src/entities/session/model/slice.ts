import { createSlice } from "@reduxjs/toolkit";
import { sessionApi } from "../api/sessionApi";

type SessionSliceState = {
  access?: string | undefined;
  refresh?: string | undefined;
  isAuthorized: boolean;
  error: string | null;
  loading: boolean;
};

const initialState: SessionSliceState = {
  isAuthorized: false,
  error: null,
  loading: false,
};

export const sessionSlice = createSlice({
  name: "session",
  initialState,
  reducers: {
    clearSessionData: (state) => {
      state.access = undefined;
      state.refresh = undefined;
      state.isAuthorized = false;
      state.error = null;
      state.loading = false;
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      sessionApi.endpoints.login.matchPending,
      (state: SessionSliceState, { payload }) => {
        state.error = null;
        state.loading = true;
      }
    );
    builder.addMatcher(
      sessionApi.endpoints.login.matchFulfilled,
      (state: SessionSliceState, { payload }) => {
        state.isAuthorized = true;
        state.error = null;
        // say TypeScript that isAuthorized = true
        if (state.isAuthorized) {
          state.access = payload.access;
          state.refresh = payload.refresh;
        }
      }
    );
    builder.addMatcher(
      sessionApi.endpoints.login.matchRejected,
      (state: SessionSliceState) => {
        state.access = undefined;
        state.refresh = undefined;
        state.isAuthorized = false;
        state.error = "Не найдено активной учетной записи с указанными данными";
        state.loading = false;
      }
    );
  },
});

export const selectIsAuthorized = (state: RootState) =>
  state.session.isAuthorized;

export const selectUserId = (state: RootState) => state.session.userId;

export const { clearSessionData } = sessionSlice.actions;
