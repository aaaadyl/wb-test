export type Session = {
  access: string;
  refresh: string;
};
