import styled from "styled-components";

export const PaginationWrapper = styled.div`
  width: 100%;
  display: flex;
  grid-gap: 5px;
  justify-content: center;
`;
