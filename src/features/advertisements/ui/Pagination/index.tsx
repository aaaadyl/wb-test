import { Button } from "@/src/shared/ui";
import { PaginationWrapper } from "./Pagination.styles";

interface PaginationProps {
  onPaginate: (page: number) => void;
  pageCount: number;
  selectedPage: number;
}

export function Pagination({
  onPaginate,
  pageCount,
  selectedPage,
}: PaginationProps) {
  return (
    <PaginationWrapper>
      {Array.from({ length: pageCount }, (_, i) => i).map((pageI) => (
        <Button
          key={pageI}
          variant={selectedPage === pageI ? "primary" : "secondary"}
          onClick={() => onPaginate(pageI)}>
          {pageI + 1}
        </Button>
      ))}
    </PaginationWrapper>
  );
}
