import styled from "styled-components";

export const SubjectWrapper = styled.div`
  height: min-content;
  display: flex;
  flex-direction: column;
  grid-gap: 20px;
  background-color: #fff;
  padding: 20px;
  border-radius: 10px;
`;

export const SubjectTitle = styled.h4`
  margin: 0;
  font-size: 14px;
  font-weight: 600;
  color: #121212;
`;

export const SubjectsList = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
  margin: 0;
  padding: 0;

  li {
    font-size: 14px;
    color: #121212;
    padding: 20px 10px;
    border-radius: 5px;

    display: grid;
    grid-template: 1fr / 50px auto;
    grid-gap: 20px;

    &:nth-child(odd) {
      border-radius: 10px;
      border: 1px solid #f3f4f5;
      box-shadow: 0px 0px 0px 0px rgba(38, 43, 48, 0.02),
        0px 0px 0px 0px rgba(38, 43, 48, 0.02),
        0px 0px 1px 0px rgba(38, 43, 48, 0.02),
        0px 0px 1px 0px rgba(38, 43, 48, 0.01),
        0px 0px 1px 0px rgba(38, 43, 48, 0), 0px 0px 1px 0px rgba(38, 43, 48, 0);
    }
  }
`;
