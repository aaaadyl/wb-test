import { useAppSelector } from "@/src/shared/model/hooks";
import { SubjectTitle, SubjectWrapper, SubjectsList } from "./Subject.styles";

export function Subjects() {
  const { priorities } = useAppSelector((state) => state.advertisements);
  return (
    <SubjectWrapper>
      {!!priorities?.length && (
        <>
          <SubjectTitle>Приоритеты категорий</SubjectTitle>

          <SubjectsList>
            {priorities.map(({ id, name }) => (
              <li key={id}>
                <span>{id}</span>
                <span>{name}</span>
              </li>
            ))}
          </SubjectsList>
        </>
      )}
    </SubjectWrapper>
  );
}
