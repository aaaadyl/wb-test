import styled from "styled-components";

export const TDContent = styled.div`
  height: 100%;
  padding: 5px 20px;

  color: #121212;
  text-align: center;
  font-size: 14px;
  font-weight: 400;
`;

export const TBody = styled.tbody`
  tr:nth-child(even) {
    background-color: #fff;

    td:first-child {
      border-radius: 10px 0 0 10px;
    }

    td:last-child {
      border-radius: 0 10px 10px 0;
    }
  }
`;
