import { AdvertDetails } from "@/src/entities/adverts";
import { type RowModel, flexRender } from "@tanstack/react-table";
import { TDContent, TBody } from "./TableBody.styles";

type TableBodyProps = {
  bodyGoup: RowModel<AdvertDetails>;
};

export function TableBody({ bodyGoup }: TableBodyProps) {
  return (
    <TBody>
      {bodyGoup.rows.map((row) => (
        <tr key={row.id}>
          {row.getVisibleCells().map((cell) => (
            <td key={cell.id}>
              <TDContent>
                {flexRender(cell.column.columnDef.cell, cell.getContext())}
              </TDContent>
            </td>
          ))}
        </tr>
      ))}
    </TBody>
  );
}
