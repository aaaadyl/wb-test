import styled from "styled-components";

export const HeadContent = styled.div`
  height: 100%;
  padding: 10px;
  font-size: 12px;
  font-weight: 400;
`;

export const THead = styled.thead`
  tr {
    background: #fff;
    border-radius: 10px;

    th:first-child {
      border-radius: 10px 0 0 10px;
    }

    th:last-child {
      border-radius: 0 10px 10px 0;
    }
  }
`;
