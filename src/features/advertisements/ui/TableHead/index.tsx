import { AdvertDetails } from "@/src/entities/adverts";
import { type HeaderGroup, flexRender } from "@tanstack/react-table";
import { HeadContent, THead } from "./TableHead.styles";

type TableColumnProps = {
  headerGoup: HeaderGroup<AdvertDetails>[];
};

export function TableHead({ headerGoup }: TableColumnProps) {
  return (
    <THead>
      {headerGoup.map((headerGroup) => (
        <tr key={headerGroup.id}>
          {headerGroup.headers.map((header) => (
            <th key={header.id}>
              {header.isPlaceholder ? null : (
                <HeadContent>
                  {flexRender(
                    header.column.columnDef.header,
                    header.getContext()
                  )}
                </HeadContent>
              )}
            </th>
          ))}
        </tr>
      ))}
    </THead>
  );
}
