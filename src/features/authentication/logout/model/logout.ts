import { createAsyncThunk } from "@reduxjs/toolkit";
import { clearSessionData, sessionApi } from "@/src/entities/session";
import { RootState } from "@/src/store/appStore";
import { SESSION_TAG } from "@/src/shared/api";

export const logoutThunk = createAsyncThunk<void, void, { state: RootState }>(
  "authentication/logout",
  async (_: unknown, { dispatch }) => {
    dispatch(clearSessionData());

    // Wait 10ms to invalidateTags in next event loop tick.
    // Otherwise after invalidate related requests with SESSION_TAG
    // will be started, but isAuthorized will still be equal to true
    await new Promise((resolve) => setTimeout(resolve, 10));

    // 👇 ATTENTION: This line clear all baseApi state instead of sessionApi
    // dispatch(sessionApi.util.resetApiState())

    dispatch(sessionApi.util.invalidateTags([SESSION_TAG]));
  }
);
