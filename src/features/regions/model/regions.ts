import { regionsApi } from "@/src/entities/regions";
import { isFetchBaseQueryError } from "@/src/shared/api";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const regionsThunk = createAsyncThunk<void, void, { state: RootState }>(
  "regions/list",
  async (_: unknown, { dispatch }) => {
    try {
      await dispatch(regionsApi.endpoints.regions.initiate()).unwrap();
    } catch (error) {
      if (isFetchBaseQueryError(error)) {
        if (typeof error.data === "string") {
          throw new Error(error.data);
        }
      }

      throw new Error("Unknown error");
    }
  }
);
