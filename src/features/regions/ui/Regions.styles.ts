import styled from "styled-components";

export const RegionsWrapper = styled.div`
  width: 100%;
  display: flex;
  grid-gap: 10px;
  align-items: center;
  overflow: auto;
`;

export const RegButton = styled.button`
  min-width: 100px;
  height: 35px;

  width: fit-content;

  color: #121212;
  font-size: 16px;

  padding: 0px 10px;
  border-radius: 4px;
  border: 1px solid #dddfe0;
  background: none;

  &.active {
    border: 1px solid #f3f4f5;
    background: #fff;
  }
`;
