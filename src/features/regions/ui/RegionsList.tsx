import { useAppDispatch, useAppSelector } from "@/src/shared/model/hooks";
import { useRegionsQuery } from "@/src/entities/regions/api/regionsApi";
import { setRegionValue } from "@/src/entities/filters";
import { Button, Spinner } from "@/src/shared/ui";
import { RegButton, RegionsWrapper } from "./Regions.styles";

export function RegionsList() {
  const dispatch = useAppDispatch();
  const { region } = useAppSelector((state) => state.filters);
  const { data: regions = [], isFetching } = useRegionsQuery();

  if (regions.length < 1) {
    return null;
  }

  return (
    <RegionsWrapper>
      {!isFetching ? (
        regions?.map(({ id, name }) => (
          <RegButton
            key={id}
            onClick={() => dispatch(setRegionValue(id))}
            className={id === region ? "active" : ""}>
            {name}
          </RegButton>
        ))
      ) : (
        <Spinner />
      )}
    </RegionsWrapper>
  );
}
