import styled from "styled-components";

export const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  grid-gap: 10px;
  border-radius: 15px;
  padding: 20px;
  margin: 0 0 20px;

  background-color: #fff;
`;

export const Title = styled.h1`
  margin: 0;
  font-family: Roboto;
  font-size: 24px;
  font-style: normal;
  font-weight: 500;
  color: #121212;

  @media screen and (max-width: 980px) {
    font-size: 24px;
  }
`;

export const SearchWrapper = styled.form`
  display: grid;
  grid-template: 1fr / 1fr 224px;
  grid-gap: 30px;

  @media screen and (max-width: 980px) {
    grid-template: 1fr / 1fr min-content;
  }
`;
