import { useState } from "react";
import { setSearchValue } from "@/src/entities/filters";
import { useAppDispatch, useAppSelector } from "@/src/shared/model/hooks";
import { MainWrapper, Title, SearchWrapper } from "./Search.styles";
import { Button, TextField } from "@/src/shared/ui";

export function Search() {
  const { loading } = useAppSelector((state) => state.advertisements);
  const [inputValue, setInputValue] = useState("");

  const dispatch = useAppDispatch();

  return (
    <MainWrapper>
      <Title>Актуальные ставки</Title>
      <SearchWrapper
        onSubmit={(event) => {
          event.preventDefault();
          dispatch(setSearchValue(inputValue));
        }}>
        <TextField
          value={inputValue}
          placeholder="Поиск по названию или артикулу"
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            setInputValue(event.target.value)
          }
        />

        <Button variant="primary" disabled={loading} type="submit">
          Найти
        </Button>
      </SearchWrapper>
    </MainWrapper>
  );
}
