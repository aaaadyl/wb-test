import { createApi } from "@reduxjs/toolkit/query/react";
import { SESSION_TAG, ADVERT_TAG, REGIONS_TAG } from "./tags";
import { baseQueryWithReauth } from "./baseQueryWithReauth";

export const baseApi = createApi({
  tagTypes: [SESSION_TAG, ADVERT_TAG, REGIONS_TAG],
  reducerPath: "api",
  baseQuery: baseQueryWithReauth,
  endpoints: () => ({}),
});
