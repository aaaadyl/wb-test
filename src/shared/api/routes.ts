export const ROUTES = {
  adverts: {
    list: `/wb/adverts/`,
  },
  regions: {
    list: `/wb/regions/`,
  },
  catalogs: {
    list: `/wb/catalogs/`,
  },
};
