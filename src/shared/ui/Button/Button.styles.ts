import { ComponentPropsWithRef } from "react";
import styled, { css } from "styled-components";

export type ButtonVariants = "primary" | "secondary" | "disabled";

interface ButtonProps extends ComponentPropsWithRef<"button"> {
  variant: ButtonVariants;
}

const background = {
  primary: css`
    background-color: #ffde31;
    color: #121212;
  `,
  secondary: css`
    background-color: #ffffdd;
    color: #1f1717;
  `,
  disabled: css`
    background-color: #eef5ff;
    color: #9bb8cd;
  `,
};

export const ButtonStyled = styled.button<ButtonProps>`
  ${({ variant }) => background[variant]}

  font-size: 16px;
  font-weight: 400;
  padding: 18px 25px;
  border-radius: 4px;
  border: none;
  cursor: pointer;
`;
