import { type ReactNode } from "react";
import { ButtonStyled, ButtonVariants } from "./Button.styles";

type Props = {
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  children: ReactNode;
  type?: "submit" | "button" | "reset";
  variant: ButtonVariants;
  isLoading?: boolean;
  disabled?: boolean;
};

export function Button({
  onClick,
  children,
  isLoading,
  disabled = false,
  type = "button",
  variant = "secondary",
}: Props) {
  return (
    <ButtonStyled
      type={type}
      variant={disabled ? "disabled" : variant}
      disabled={disabled}
      onClick={onClick}>
      {isLoading ? <span>loading...</span> : <>{children}</>}
    </ButtonStyled>
  );
}
