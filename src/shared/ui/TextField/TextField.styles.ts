import styled from "styled-components";

export const Input = styled.input`
  width: 100%;
  padding: 18px 4px;

  background: none;
  border: none;
  border-bottom: 1px solid #777;

  font-size: 16px;
  font-style: normal;
  font-weight: 400;
  color: #121212;

  &::placeholder {
    color: #777;
  }
`;
