import { InputHTMLAttributes } from "react";
import { Input } from "./TextField.styles";

interface TextFieldProps extends InputHTMLAttributes<HTMLInputElement> {}

export function TextField(props: TextFieldProps) {
  return <Input {...props} />;
}
