import { combineReducers } from "@reduxjs/toolkit";
import { baseApi } from "../shared/api";
import { sessionSlice } from "../entities/session";
import { filtersSlice } from "../entities/filters";
import { advertisementsSlice } from "../entities/adverts/model/slice";

export const rootReducer = combineReducers({
  [sessionSlice.name]: sessionSlice.reducer,
  [baseApi.reducerPath]: baseApi.reducer,
  [filtersSlice.name]: filtersSlice.reducer,
  [advertisementsSlice.name]: advertisementsSlice.reducer,
});
