import styled from "styled-components";

export const MainWrapper = styled.div`
  width: 100%;
  min-height: 100vh;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const LoginForm = styled.form`
  max-width: 500px;
  width: 100%;
  height: min-content;

  display: flex;
  flex-direction: column;
  grid-gap: 30px;

  background-color: #fff;

  padding: 30px 25px;
  border-radius: 10px;
`;

export const Title = styled.h1``;

export const Error = styled.p`
  color: #777;
  text-align: center;
  font-size: 14px;
  color: #ff004d;
  margin: 0;
`;
