"use client";

import { useEffect, type FormEvent } from "react";
import { Button, TextField } from "@/src/shared/ui";
import { Title, LoginForm, MainWrapper, Error } from "./LoginPage.styles";
import { useAppDispatch, useAppSelector } from "@/src/shared/model/hooks";
import { clearSessionData, sessionApi } from "@/src/entities/session";
import AuthGuard from "@/src/widgets/auth-guard";

export default function LoginPage() {
  const dispatch = useAppDispatch();
  const { error, loading } = useAppSelector((state) => state.session);
  useEffect(() => {
    dispatch(clearSessionData());
  }, []);

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const formElements = event.currentTarget.elements;
    const email =
      (formElements.namedItem("email") as HTMLInputElement)?.value || "";
    const password =
      (formElements.namedItem("password") as HTMLInputElement)?.value || "";
    dispatch(sessionApi.endpoints.login.initiate({ email, password }));
  };

  return (
    <AuthGuard>
      <MainWrapper>
        <Title>Login</Title>
        <LoginForm onSubmit={handleSubmit}>
          <TextField placeholder="Почта" name="email" required type="string" />
          <TextField
            placeholder="Пароль"
            name="password"
            required
            type="password"
          />
          <Button disabled={loading} variant="primary" type="submit">
            {loading ? "...loading" : "Войти"}
          </Button>

          <Error>{error}</Error>
        </LoginForm>
      </MainWrapper>
    </AuthGuard>
  );
}
