"use client";

import { Search } from "@/src/features/search";
import { AdvertsTable } from "@/src/widgets/advertisements";
import { HeaderLayout } from "@/src/widgets/header-layout";

export default function MainPage() {
  return (
    <HeaderLayout>
      <Search />
      <AdvertsTable />
    </HeaderLayout>
  );
}
