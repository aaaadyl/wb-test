import styled from "styled-components";
import Image from "next/image";

interface TitleProps {
  error: boolean;
}

export const Title = styled.p<TitleProps>`
  margin: 0;
  font-size: 18px;
  text-align: center;
  color: ${({ error }) => (error ? "#FF004D" : "#1D2B53")};
`;

export const MainWrapper = styled.div`
  width: 100%;
  display: grid;
  grid-template: 1fr / 2fr 1fr;
  grid-gap: 20px;

  @media screen and (max-width: 1200px) {
    display: flex;
    flex-direction: column-reverse;
  }
`;

export const TableWrapper = styled.div`
  width: 100%;
  overflow: auto;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

export const SomeText = styled.p`
  color: #777777;
  font-size: 16px;
  font-weight: 400;
  margin: 0;
`;

export const LeftWrapper = styled.div`
  padding-top: 20px;
  display: flex;
  flex-direction: column;
  grid-gap: 20px;
`;

export const AdvertImage = styled(Image)`
  object-fit: contain;
`;

export const Article = styled.p`
  color: #2c80ff;
  font-size: 14px;
  font-weight: 400;
  text-decoration-line: underline;
`;

export const Cpm = styled.div`
  border-radius: 10px;
  background: rgba(255, 222, 49, 0.2);
  padding: 10px;
`;
