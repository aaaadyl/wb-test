import { useState, useEffect } from "react";
import {
  createColumnHelper,
  getCoreRowModel,
  useReactTable,
  getPaginationRowModel,
} from "@tanstack/react-table";
import { useAppSelector } from "@/src/shared/model/hooks";
import { useAdvertisementsQuery } from "@/src/entities/adverts/api/advertApi";
import { Spinner } from "@/src/shared/ui";
import { AdvertDetails } from "@/src/entities/adverts";
import {
  Pagination,
  Subjects,
  TableBody,
  TableHead,
} from "@/src/features/advertisements";
import {
  MainWrapper,
  Table,
  TableWrapper,
  LeftWrapper,
  Title,
  SomeText,
  AdvertImage,
  Article,
  Cpm,
} from "./AdvertsTable.styles";
import { RegionsList } from "@/src/features/regions";

const columnHelper = createColumnHelper<AdvertDetails>();

const columns = [
  columnHelper.accessor("place", {
    id: "place",
    header: () => "Место",
    cell: (info) => info.getValue(),
  }),
  columnHelper.accessor("image", {
    id: "image",
    cell: (info) => (
      <AdvertImage
        src={info.getValue()}
        width={65}
        height={65}
        objectFit="contain"
        alt={`${info.column.id}.jpg`}
      />
    ),
    header: () => "Фото",
  }),
  columnHelper.accessor("article", {
    id: "article",
    header: () => "Артикл",
    cell: (info) => <Article>{info.getValue()}</Article>,
  }),
  columnHelper.accessor("position", {
    id: "position",
    header: () => "Позиция",
    cell: (info) => info.getValue(),
  }),
  columnHelper.accessor("cpm", {
    id: "cpm",
    header: () => "Ставка",
    cell: (info) => <Cpm>{info.getValue()}</Cpm>,
  }),
  columnHelper.accessor((row) => row?.subject?.name, {
    id: "subject",
    header: () => "Категория",
    cell: (info) => info.getValue(),
  }),
  columnHelper.accessor("delivery", {
    id: "delivery",
    header: () => "Доставка",
    cell: (info) => info.getValue(),
  }),
];

export function AdvertsTable() {
  const { region, search } = useAppSelector((state) => state.filters);
  const [pageCount, setPageCount] = useState(1);
  const { adverts, loading, error } = useAppSelector(
    (state) => state.advertisements
  );

  const { refetch } = useAdvertisementsQuery({
    region_id: region,
    input: search,
  });

  useEffect(() => {
    refetch();
  }, [region, search]);

  const table = useReactTable({
    data: adverts,
    columns: columns,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });

  useEffect(() => {
    const _pageCount = Array.from(
      new Set(adverts.map((advert) => advert.page))
    )?.length;

    table.setPageSize(24);
    table.setPageCount(_pageCount);
    setPageCount(_pageCount);
  }, [adverts]);

  if (!search || error) {
    return (
      <MainWrapper>
        <Title error={!search || !!error}>
          {(!search && "Введите поиск чтобы вывести ставки!") || error}
        </Title>
      </MainWrapper>
    );
  }

  const renderTable = (
    <TableWrapper>
      <Table>
        <TableHead headerGoup={table.getHeaderGroups()} />
        <TableBody bodyGoup={table.getRowModel()} />
      </Table>
    </TableWrapper>
  );

  return (
    <MainWrapper>
      <LeftWrapper>
        <RegionsList />
        <SomeText>
          Est ipsum gravida sit non. Mi ac habitasse proin sollicitudin
          malesuada blandit. Arcu turpis cursus imperdiet diam tincidunt augue
          ut. Metus proin vel consectetur ipsum quis amet faucibus mus. Placerat
          cras ac amet dictum. Massa sed cursus dapibus morbi turpis velit id
          mauris at.
        </SomeText>
        {loading && <Spinner />}
        {!!adverts?.length && (
          <>
            {renderTable}
            <Pagination
              onPaginate={table.setPageIndex}
              selectedPage={table.getState().pagination.pageIndex}
              pageCount={pageCount}
            />
          </>
        )}
      </LeftWrapper>
      <Subjects />
    </MainWrapper>
  );
}
