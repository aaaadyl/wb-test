import { type ReactNode, useEffect } from "react";
import { useRouter } from "next/navigation";
import { useAppSelector } from "@/src/shared/model/hooks";
import { paths } from "@/src/shared/lib";

type AuthGuardProps = {
  children: ReactNode;
};

export default function AuthGuard({ children }: AuthGuardProps) {
  const { access, isAuthorized } = useAppSelector((state) => state.session);
  const router = useRouter();

  useEffect(() => {
    if (!isAuthorized) {
      router.push(paths.LOGIN);
    } else if (access) {
      router.push(paths.ROOT);
    }
  }, [access, isAuthorized]);
  return <>{children}</>;
}
