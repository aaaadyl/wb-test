import styled from "styled-components";

export const MainWrapper = styled.div`
  max-width: 1260px;
  width: 100%;
  margin: 0 auto;
  padding: 0 20px 20px;
`;

export const Header = styled.header`
  height: 100px;
  display: flex;
  grid-gap: 30px;
  align-items: center;
  margin-bottom: 40px;
`;
