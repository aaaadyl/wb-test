import styled from "styled-components";

export const Wrapper = styled.div`
  display: grid;
  grid-template: 1fr / repeat(4, min-content);
  grid-gap: 30px;
  align-items: center;
`;

export const Items = styled.div`
  color: #777;
  font-size: 14px;
  font-weight: 400;
  cursor: pointer;
`;

export const ProfileBlock = styled.div`
  display: flex;
  grid-gap: 2px;
  align-items: center;
  justify-content: center;
`;
