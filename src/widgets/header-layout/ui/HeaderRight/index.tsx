import Image from "next/image";
import { Items, ProfileBlock, Wrapper } from "./HeaderRight.styles";

export function HeaderRight() {
  return (
    <Wrapper>
      <Items>Поддержка</Items>
      <Items>Тарифы</Items>
      <Image src="/bell.svg" width={24} height={24} alt="notification.ico" />
      <ProfileBlock>
        <Image src={"ava.png"} width={50} height={50} alt="profile image" />
        <Image src="/arrow-down.svg" width={24} height={24} alt="arrow-down" />
      </ProfileBlock>
    </Wrapper>
  );
}
