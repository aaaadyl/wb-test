import Image from "next/image";

export function Logo() {
  return (
    <div>
      <Image src={"/logo.svg"} width={244} height={28} alt="logotype" />
    </div>
  );
}
