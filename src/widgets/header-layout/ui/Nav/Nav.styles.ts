import styled from "styled-components";

export const NavWrapper = styled.nav`
  width: 100%;
  display: flex;
  grid-gap: 30px;
`;

export const NavItem = styled.div`
  width: fit-content;
  display: flex;
  grid-gap: 2px;
  align-items: center;

  color: #777;
  font-size: 14px;
  font-weight: 400;
  cursor: pointer;

  border-bottom: 2px solid transparent;

  &.active {
    color: #121212;
    border-bottom: 2px solid #ffde31;
  }
`;
