import { NavItems } from "../types";

export const NAV_ITEMS: NavItems[] = [
  { title: "Продавцы" },
  { title: "Мои кампании", children: [] },
  { title: "Актуальные ставки" },
  { title: "Возможности", children: [] },
];
