import Image from "next/image";

import { NavItem, NavWrapper } from "./Nav.styles";
import { NAV_ITEMS } from "./config";

export function Nav() {
  return (
    <NavWrapper>
      {NAV_ITEMS.map((item, i) => (
        <NavItem key={item.title} className={`${i == 1 ? "active" : ""}`}>
          <span>{item.title}</span>
          {!!item.children && (
            <Image
              src={"/arrow-down.svg"}
              width={24}
              height={24}
              alt="arrow-down"
            />
          )}
        </NavItem>
      ))}
    </NavWrapper>
  );
}
