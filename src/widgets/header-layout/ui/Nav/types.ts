export interface NavItems {
  title: string;
  children?: NavItems[] | [];
}
