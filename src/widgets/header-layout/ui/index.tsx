import { ReactNode } from "react";
import { Header, MainWrapper } from "./HeaderLayout.styles";
import { Logo } from "./Logo";
import { Nav } from "./Nav";
import { HeaderRight } from "./HeaderRight";

type HeaderLayoutProps = {
  children: ReactNode;
};

export function HeaderLayout({ children }: HeaderLayoutProps) {
  return (
    <MainWrapper>
      <Header>
        <Logo />
        <Nav />
        <HeaderRight />
      </Header>
      {children}
    </MainWrapper>
  );
}
